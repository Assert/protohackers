import socket
import threading

HOSTNAME = socket.gethostbyname(socket.gethostname())
PORT = 1337
ADDRESS = (HOSTNAME, PORT)
FORMAT = "utf-8"

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDRESS)


def handle_connection(conn, addr) -> None:
    frags = []
    while True:
        chunk = conn.recv(1024)
        if not chunk:
            break
        frags.append(chunk)
    msg = b"".join(frags)
    conn.sendall(msg)
    conn.close()


def main() -> None:
    server.listen()
    print(f"{HOSTNAME}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_connection, args=(conn, addr))
        thread.start()
        print(f"CURRENT CONNETIONS {threading.active_count() - 1}")


if __name__ == "__main__":
    print("SERVER UP")
    main()
