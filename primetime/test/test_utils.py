from src.utils import *
import pytest


@pytest.mark.parametrize(
    "number, expected", [("123.123", False), ("1", False), ("2", True), ("-2", False)]
)
def test_is_prime(number, expected):
    assert is_prime(number) == expected


def test_is_correct_form():
    req1 = {}
    req1["lmao"] = None
    req1["lol"] = None
    want = False
    got = is_correct_form(req1)
    assert got == want

    req2 = {}
    req2["number"] = 13
    req2["method"] = "isPrime"
    want = True
    got = is_correct_form(req2)
    assert got == want

    req3 = {}
    req3["number"] = 7
    want = False
    got = is_correct_form(req3)
    assert got == want

    req4 = {}
    req4["method"] = "isPrime1"
    want = False
    got = is_correct_form(req4)
    assert got == want

    req5 = {}
    req5["number"] = 10
    req5["method"] = "isPrime"
    want = True
    got = is_correct_form(req5)
    assert got == want
