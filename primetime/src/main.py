import json
import socket
import threading
import utils

HOSTNAME = socket.gethostbyname(socket.gethostname())
PORT = 1337
ADDRESS = (HOSTNAME, PORT)
FORMAT = "utf-8"

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDRESS)


def handle_connection(conn, addr) -> None:
    while True:
        frags = []
        while True:
            chunk = conn.recv(1024)
            if not chunk:
                break
            frags.append(chunk)
        msg = b"".join(frags)
        frags = []
        try:
            _json = json.loads(msg)
        except json.JSONDecodeError:
            _json = {}
        print(_json)
        if utils.is_correct_form(_json):
            response = {}
            response["method"] = "isPrime"
            if utils.is_prime(_json["number"]):
                response["prime"] = True
            else:
                response["prime"] = False
            conn.sendall(json.dumps(response).encode())
            print("sent correct")

        else:
            malformed = {"hurr": "derp"}
            conn.sendall(json.dumps(malformed).encode())
            conn.close()
            return


def main() -> None:
    server.listen()
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_connection, args=(conn, addr))
        thread.start()


if __name__ == "__main__":
    print("SERVER UP")
    main()
