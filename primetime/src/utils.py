"""
utils.py
Helpers for Protohackers: Prime Time
"""
from math import sqrt
from numbers import Number


def is_prime(number: str) -> bool:
    """
    try to cast to int then check if prime, otherwise not prime
    """
    try:
        n = int(number)
        root = int(sqrt(n))
        if n <= 1:
            return False

        for i in range(2, int(sqrt(n)) + 1):
            if n % i == 0:
                return False
        return True

    except ValueError:
        return False


def is_correct_form(req) -> bool:
    """
    looking for keys: "method" and "number"
    method's value must always be "isPrime"
    number must contain a valid JSON number
    """
    if "method" not in req or "number" not in req:
        return False

    if req["method"] != "isPrime":
        return False

    if not isinstance(req["number"], Number):
        return False

    return True
